﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ШП_ЛР1
{
    public class ErrorTracker
    {
        private static ErrorTracker _instance;
        private List<ErrorEntry> _errorHistory;
        private readonly string _logFilePath = "error_log.txt";

        private ErrorTracker()
        {
            _errorHistory = new List<ErrorEntry>();
        }

        public static ErrorTracker Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ErrorTracker();
                }
                return _instance;
            }
        }

        public void LogError(string errorCode, string errorDescription)
        {
            ErrorEntry errorEntry = new ErrorEntry
            {
                TimeStamp = DateTime.Now,
                ErrorCode = errorCode,
                ErrorDescription = errorDescription
            };

            _errorHistory.Add(errorEntry);
        }

        public void ClearErrorHistory()
        {
            _errorHistory.Clear();
        }

        public void PrintErrorHistory()
        {
            Console.WriteLine("Error History:");
            foreach (var errorEntry in _errorHistory)
            {
                Console.WriteLine($"{errorEntry.TimeStamp} - [{errorEntry.ErrorCode}] {errorEntry.ErrorDescription}");
            }
        }

        public void SaveErrorHistoryToFile()
        {
            using (StreamWriter writer = new StreamWriter(_logFilePath, true))
            {
                foreach (var errorEntry in _errorHistory)
                {
                    writer.WriteLine($"{errorEntry.TimeStamp} - [{errorEntry.ErrorCode}] {errorEntry.ErrorDescription}");
                }
            }
            Console.WriteLine($"Error history saved to {_logFilePath}");
        }
    }

    public class ErrorEntry
    {
        public DateTime TimeStamp { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            ErrorTracker errorTracker = ErrorTracker.Instance;

            var error1 = Activator.CreateInstance(typeof(ErrorEntry)) as ErrorEntry;
            error1.TimeStamp = DateTime.Now;
            error1.ErrorCode = "ERR001";
            error1.ErrorDescription = "Помилка вводу даних";

            var error2 = Activator.CreateInstance(typeof(ErrorEntry)) as ErrorEntry;
            error2.TimeStamp = DateTime.Now;
            error2.ErrorCode = "ERR002";
            error2.ErrorDescription = "Помилка з'єднання з сервером";

            errorTracker.LogError(error1.ErrorCode, error1.ErrorDescription);
            errorTracker.LogError(error2.ErrorCode, error2.ErrorDescription);

            errorTracker.PrintErrorHistory();
            errorTracker.SaveErrorHistoryToFile();
            errorTracker.ClearErrorHistory();
        }
    }
}
